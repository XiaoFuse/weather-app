# Weather app

Weather app does provide current weather information and a weather forecast for your mobile device running [Ubuntu Touch](https://ubuntu-touch.io/).

<img src="screenshot_readme.png" alt="screenshot" width="182"/>

&nbsp;

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/com.ubuntu.weather)

## Ubuntu Touch

This app is a core app for [Ubuntu Touch](https://ubuntu-touch.io/).
Ubuntu Touch is a mobile OS developed by [UBports](https://ubports.com/).
A group of volunteers and passionate people across the world.
With Ubuntu Touch we offer a truly unique mobile experience - an alternative to the current most popular operating systems on the market.
We believe that everyone is free to use, study, share and improve all software created by the foundation without restrictions.
Whenever possible, everything is distributed under free and open source licenses endorsed by the Free Software Foundation, the Open Source Initiative.

## Reporting Bugs and Requesting Features

Bugs and feature requests can be reported via our [bug tracker](https://gitlab.com/ubports/apps/weather-app/issues).

## Translating

This app can be translated on the [UBports Weblate](https://translate.ubports.com/projects/ubports/weather-app/).
We welcome translators from all different languages. Thank you for your contribution!

To add a new language, log into weblate, goto *tools* --> *start new translation*.

## Developing

1. Install [Clickable](http://clickable.bhdouglass.com/en/latest/)

2. Fork [weather app at gitlab](https://gitlab.com/ubports/apps/weather-app) into your own namespace. You may need to open an account with gitlab if you do not already have one.

2. Open a terminal by pressing: `ctl + alt + t` (ubuntu).

3. Clone the repo onto your local machine using git:

    `git clone https://gitlab.com/ubports/apps/weather-app.git`.

4. Change into weather apps folder: `cd weather-app`.

5. Run `clickable` from the root of this repository to build and deploy weather-app on your attached Ubuntu Touch device, run `clickable desktop` to test weather app on your desktop, run `clickable log` for debug information

### API
Weather app currently supports only weather data from [OpenWeatherMap](https://openweathermap.org/).

The OpenWeatherMap service requires an API key.
Visit [OpenWeatherMap - API](https://openweathermap.org/api) for detailed information about the API and how to obtain a personal key. Get your key and place it between the quotation marks for the *\"owmKey"* variable in *app/data/keys.js*.

**Do not commit branches with private keys in place! A centrally managed key is injected at build time.**

*Note: Some services may require a paid plan.*

### Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, Matrix ( #ubcd:matrix.org ), or any other method with the owners of this repository before making a change.
Please note we have a [code of conduct](https://ubports.com/foundation/ubports-foundation/foundation-codeofconduct), please follow it in all your interactions with the project.

### Maintainer

This Ubuntu Touch core app is being maintained by [Daniel Frost](https://gitlab.com/Danfro).

See also the list of [contributors](https://gitlab.com/ubports/apps/weather-app/-/graphs/master) who participated in this project.

### Releasing

New releases are automatically pushed to the OpenStore via the GitLab CI. To make a new release a maintainer must:

- Increment the version in the manifest.json
- Create a new git commit, the commit message will become part of the automatic changelog in the OpenStore
- Tag the commit with a tag starting with `v` (for example: `git tag v1.2.3`)
- Push the new commit and tag to GitLab: `git push && git push --tags`
- The GitLab CI will build and publish the new release to the OpenStore

### Useful Links

- [UBports App Dev Docs](http://docs.ubports.com/en/latest/appdev/index.html)
- [UBports SDK Docs](https://api-docs.ubports.com/)
- [UBports](https://ubports.com/)
- [Ubuntu Touch](https://ubuntu-touch.io/)
- [OpenWeatherMap API](https://openweathermap.org/api)

## Donating

If you love UBports and it's apps please consider dontating to the [UBports Foundation](https://ubports.com/donate). Thanks in advance for your generous donations.

## License

Copyright 2019 [UBports Foundation](https://ubports.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.
